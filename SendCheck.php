<?php

class SendCheck{
    private $sess;
    private $mode = "smtp";
    private $host = "";
    private $user = "";
    private $pass = "";
    private $from = "";
    private $to = "";
    private $subject = "";
    private $body = "";

    private $modeList = ["smtp" => 25,"smtpauth" => 587];

    private $showBuf = false;
    private $mess = [
        "connect"       =>"<br>■サーバー接続<br>",
        "login"         =>"<br>■サーバーログイン<br>",
        "send"          =>"<br>■メール送信<br>",
        "close"         =>"<br>■サーバー切断<br>",
        "get"           =>"<br>受信：",
        "send"          =>"<br>送信："
        ];
    private $emess = [
        "connect"       =>"Server Connection ERROR",
        "login"         =>"Login ERROR",
        "send"         =>"Mail Check ERROR",
        "close"         =>"Server Close ERROR"
        ];
    
    private $throw = true;
    private $buf = false;
    private $mbuf = array();

  
    /**
     * 
     * @param boolean $change
     * @return boolean
     */
    public function setDebug($change){
        $this->showBuf = (is_bool($change))?$change:true;
        return false;
    }
    /**
     * 
     * @param type $change
     * @return boolean
     */
    public function setThrow($change){
        $this->throw = (is_bool($change))?$change:true;
        return false;
    }
    
    
    
    /**
     * サーバー接続
     * @param string $host
     * @param string $user
     * @param string $pass
     * @param string $mode
     * @return boolean
     */
    public function open($host,$user,$pass,$mode="smtp"){
        $this->mode = $mode;
        $this->host = $host;
        $this->user = $user;
        $this->pass = $pass;

        if($this->connect($host,$this->modeList[$mode])){
            return true;
        }else{
            $this->error($this->emess["connect"]);
            return false;
        }
    }
    /**
     * メール情報設定
     * @param string $from      //送信元
     * @param string $to        //送信先
     * @param string $subject   //タイトル
     * @param string $body      //本文
     */
    public function setMail($from,$to,$subject,$body){
        $this->from    = ($from    != null)?$from   :"";
        $this->to      = ($to      != null)?$to     :"";
        $this->subject = ($subject != null)?$subject:"";
        $this->body    = ($body    != null)?$body   :"";
    }
    
    /**
     * SMTP認証
     * @return boolean
     */
    public function auth(){
        $func = "status_".$this->mode;
        $check =  $this->$func();
        if($check){
            return true;
        }else{
            $this->error($this->emess["login"]);
            return false;
        }
    }
    
    /**
     * メール送信
     * @return boolean
     */
    public function send(){
        $func = "send_".$this->mode;
        $check =  $this->$func();
        if($check){
            return true;
        }else{
            $this->error($this->emess["send"]);
            return false;
        }
    }
    /**
     * SMTP認証単体試験
     * @return boolean
     */
    public function login_test(){
        if($this->mode==="smtpauth"){
            if($this->login()){
                return true;
            }
            return false;
        }
    }
    /**
     * メール送信単体試験
     * @return boolean
     */
    public function send_test(){
        if($this->mode==="smtpauth"){
            if($this->setEnvelope()){
                if($this->setBody()){
                    return true;
                }
            }
            return false;
        }
    }
    /**
     * サーバー切断
     * @return boolean
     */
    public function close(){
        $func = "close_".$this->mode;
        $check =  $this->$func();
        if($check){
            return true;
        }else{
            $this->error($this->emess["close"]);
            return false;
        }
    }

    /**
    **   サーバーへ接続
    **/
    private function connect($host,$port){
        $this->sess = fsockopen($host,$port,$err,$errno,10);
        $this->bcheck = true;
        if(!$this->check_response('220')){
            $this->close();
            return false;
        }else{
            $this->setBuf("EHLO {$this->host}\r\n");
            if(!$this->check_response('250',true)){
                return false;
            }
            return true;
        }
    }
    
    private function send_smtp(){
        if($this->setEnvelope()){
            if($this->setBody()){
                return true;
            }
        }
        return false;
    }
    private function send_smtpauth(){
        if($this->login()){
            if($this->setEnvelope()){
                if($this->setBody()){
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
    **  ログイン
    **/
    private function login(){
        $this->setBuf("AUTH LOGIN\r\n");
        if(!$this->check_response('334')){
            return false;
        }
        $this->setBuf(base64_encode($this->user)."\r\n");
        if(!$this->check_response('334')){
            return false;
        }
        $this->setBuf(base64_encode($this->pass)."\r\n");
        if(!$this->check_response('235')){
            return false;
        }
        return true;
    }

    /**
     * メールヘッダー作成
     * @return boolean
     */
    private function setEnvelope(){
        $this->setBuf("MAIL FROM: {$this->from}\r\n");
        if(!$this->check_response('250')){
            return false;
        }
        $this->setBuf("RCPT TO: {$this->to}\r\n");
        if(!$this->check_response('250')){
            return false;
        }
        return true;
    }
    /**
     * メール内容作成、送信
     * @return boolean
     */
    private function setBody(){
        $this->setBuf("DATA \r\n");
        if(!$this->check_response('354')){
            return false;
        }
        $this->setBuf("Subject: {$this->subject}\r\n");
        $this->setBuf("To: {$this->to}\r\n");
        $this->setBuf("{$this->body}\r\n");
        $this->setBuf(".\r\n");
        if(!$this->check_response('250')){
            return false;
        }
        
        return true;
    }
  
    /*
     * セッション終了
     */
    private function close_smtp(){
        $this->setBuf("QUIT \r\n");
        if(!$this->check_response('221')){
            return false;
        }
        return true;
    }
  
    private function close_smtpauth(){
        $this->setBuf("QUIT \r\n");
        if(!$this->check_response('221')){
            return false;
        }
        return true;
    }
  
  
    /**
    **　レスポンスチェック
    **/
    private function check_response($num='250',$m=false){
        if(!$m){
            $this->getBuf();
            if(strpos($this->buf,$num) !== false){
              return true;
            }
        }else{
            $this->getBuf_multi();

            for($i=0;$i<=count($this->mbuf);$i++){
                if(strpos($this->mbuf[$i],$num) !== false){
                    $this->mbuf=array();
                    return true;
                }
            }
            $this->mbuf=array();
        }
        return false;
    }
    private $bcheck = false;
    /**
     * 
     * @str type string
     * バッファに書き込み
     */
    private function setBuf($str){
        fwrite($this->sess,$str);
        $this->bcheck = true;
        if($this->showBuf){
            print($this->mess["send"].$str."<br>");
        }
    }
    /**
     * バッファを取得
     */
    private function getBuf(){
        if($this->bcheck){
            $this->buf = fgets($this->sess,512);
            $this->bcheck = false;
            if($this->showBuf){
                print($this->mess["get"].$this->buf."<br>");
            }
        }
        return false;
    }
    /**
     * バッファを取得 複数行
     */
    private function getBuf_multi(){
        if($this->bcheck){
            $this->bcheck = false;
            while($msg=fgets($this->sess)){
                if($this->showBuf){
                    print($this->mess["get"].$msg."<br>");
                }
                array_push($this->mbuf,$msg);
                if(strpos($msg,"250 ") !== false){
                    break;
                }
            }
        }
        return false;
    }
}

 ?>
