<?php
//include_once 'PHPUnit/Autoload.php';
include_once"../ReceptionCheck.php";

class ReceptionTest extends PHPUnit_Framework_TestCase
{
    private $obj;
    private $host    = 'tcp://192.168.2.197';
    private $user    = 'infotec';
    private $pass    = 'mae32makase';
    private $mode1    = 'pop3';
    private $mode2    = 'imap';
    
    /**
     * 事前処理
     */
    public function setUp(){
        //サーバー接続
        $this->obj = new ReceptionCheck();        
    }
    /**
     * 事後処理
     */
    public function tearDown(){
        $this->obj->close();
    }
    /**
     * サーバー接続試験
     * @throws Exception
     */
    public function testPop3Connect(){
        
        if($this->obj->open(
                    $this->host,
                    $this->user,
                    $this->pass,
                    $this->mode1)){
            $this->assertTrue(true);
        }else{
            throw new Exception('Server Connection Error');
        }
        
    }
    
    /**
     * メールチェック試験
     * @throws Exception
     */
    public function testPop3Check(){
 
        $this->testPop3Connect();
                
        //メールチェック
        if(is_numeric($this->obj->check())){
            $this->assertTrue(true);
        }else{
            throw new Exception('Mail check ERROR');
        }
    }
    /**
     * メール読み込み試験
     * @throws Exception
     */
    public function testPop3Read(){
        
        $this->testPop3Connect();
                
        //メールチェック
        if(is_numeric($this->obj->check())){
            $this->assertTrue(true);
        }else{
            throw new Exception('Mail check ERROR');
        }
        
        //メール確認
        if(is_string($this->obj->readMail(1))){
            $this->assertTrue(true);
        }else{
            throw new Exception('Read Mail ERROR');
        }
    }

    
        /**
     * サーバー接続試験
     * @throws Exception
     */
    public function testIMAPConnect(){
        
        if($this->obj->open(
                    $this->host,
                    $this->user,
                    $this->pass,
                    $this->mode2)){
            $this->assertTrue(true);
        }else{
            throw new Exception('Server Connection Error');
        }
        
    }
    
    /**
     * メールチェック試験
     * @throws Exception
     */
    public function testIMAPCheck(){
         $this->testIMAPConnect();
               
        //メールチェック
        if(is_numeric($this->obj->check())){
            $this->assertTrue(true);
        }else{
            throw new Exception('Mail check ERROR');
        }
    }
    /**
     * メール読み込み試験
     * @throws Exception
     */
    public function testIMAPRead(){
        
        $this->testIMAPConnect();
                
        //メールチェック
        if(is_numeric($this->obj->check())){
            $this->assertTrue(true);
        }else{
            throw new Exception('Mail check ERROR');
        }
        
        //メール確認
        if(is_string($this->obj->readMail(1))){
            $this->assertTrue(true);
        }else{
            throw new Exception('Read Mail ERROR');
        }
    }
}



