<?php
include_once 'PHPUnit/Autoload.php';
include_once"../SendCheck.php";

class SendTest extends PHPUnit_Framework_TestCase
{
    private $obj;
    private $host    = 'tcp://192.168.2.197';
    private $user    = 'infotec';
    private $pass    = 'mae32makase';
    private $from    = 'infotec@neo.winwin.ne.jp';
    private $to      = 'ton_ma@hotmail.com';
    private $subject = 'test';
    private $body    = 'test';
    private $mode    = ['smtp','smtpauth'];
    
    /**
     * 事前処理
     */
    public function setUp(){
        //サーバー接続
        $this->obj = new SendCheck();        
    }
    /**
     * 事後処理
     */
    public function tearDown(){
        $this->obj->close();
    }
    /**
     * 
     * @throws Exception
     */
    public function testSMTPConnect(){
        
        if($this->obj->open(
                    $this->host,
                    $this->user,
                    $this->pass,
                    $this->mode[0])){
            $this->assertTrue(true);
        }else{
            throw new Exception('Server Connection Error');
        }
    }
    
    public function testSMTPSend(){

        $this->testSMTPConnect();
                
        $this->obj->setMail(
                        $this->from,
                        $this->to,
                        $this->subject,
                        $this->body);
        $check = $this->obj->send();
        if($check){
            $this->assertTrue(true);
        }else{
            throw new Exception('Server Connection Error');
        }
    }
    

    /**
     * 
     * @throws Exception
     */
    public function testSMTPAUTHConnect(){
        
        if($this->obj->open(
                    $this->host,
                    $this->user,
                    $this->pass,
                    $this->mode[1])){
            $this->assertTrue(true);
        }else{
            throw new Exception('Server Connection Error');
        }
    }
    
    public function testSMTPAUTHLogin(){
        $this->testSMTPAUTHConnect();
        
        if($this->obj->login_test()){
            $this->assertTrue(true);
        }else{
            throw new Exception('Login Error');
        }
        
    }
    
    public function testSMTPAUTHSend(){

        $this->testSMTPAUTHConnect();
                
        if($this->obj->login_test()){
            $this->assertTrue(true);

            $this->obj->setMail(
                            $this->from,
                            $this->to,
                            $this->subject,
                            $this->body);
            $check = $this->obj->send_test();
            if($check){
                $this->assertTrue(true);
            }else{
                throw new Exception('Mail Send Error');
            }
        }else{
            throw new Exception('Login Error');
        }
    }
}



