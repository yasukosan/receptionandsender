<?php

class ReceptionCheck{
    private $sess;
    private $mode = "pop3";
    private $user = "";
    private $pass = "";

    private $modeList = ["pop3" => 110,"imap" => 143];

    private $showBuf = false;
    private $mess = [
        "connect"       =>"<br>■サーバー接続<br>",
        "login"         =>"<br>■サーバーログイン<br>",
        "mail_check"    =>"<br>■メール確認<br>",
        "mail_read"     =>"<br>■メール読み込み<br>",
        "mail_delete"   =>"<br>■メール削除<br>",
        "close"         =>"<br>■サーバー切断<br>",
        "get"           =>"<br>受信：",
        "send"          =>"<br>送信："
        ];
    private $emess = [
        "connect"       =>"Server Connection ERROR",
        "login"         =>"Login ERROR",
        "check"         =>"Mail Check ERROR",
        "read"          =>"Mail Read ERROR",
        "close"         =>"Server Close ERROR"
        ];
    
    private $throw = true;
    private $buf = false;

    /**
     * 
     * @param boolean $change
     * @return boolean
     */
    public function setDebug($change){
        $this->showBuf = (is_bool($change))?$change:true;
        return false;
    }
    /**
     * 
     * @param type $change
     * @return boolean
     */
    public function setThrow($change){
        $this->throw = (is_bool($change))?$change:true;
        return false;
    }
    
    /**
     * ホスト名                 $host
     * ログインユーザー         $user
     * ログインパスワード       $pass
     * 接続モード(pop3 or imap) $mode
     * @param string $host
     * @param string $user
     * @param string $pass
     * @param string $mode
     * @return boolean
     */
    public function open($host,$user,$pass,$mode="pop3"){
        $this->mode = $mode;
        $this->user = $user;
        $this->pass = $pass;
        
        if($this->showBuf)print($this->mess['connect']);
        if($this->connect($host,$this->modeList[$mode])){
            if($this->showBuf)print($this->mess['login']);
            if($this->login()){
                return true;
            }else{
                $this->error($this->emess["login"]);
                return false;
            }
        }else{
            $this->error($this->emess["connect"]);
            return false;
        }
    }
    
   
    /**
     * メールチェック
     * @return boolean
     */
    public function check(){
        if($this->showBuf)print($this->mess['mail_check']);
        $func = "status_".$this->mode;
        $num = $this->$func();
        if(is_numeric($num)){
            return $num;
        }else{
            $this->error($this->emess["check"]);
            return false;
        }
    }
    
    /**
     * メール読み込み
     * @param string $num
     * @return boolean
     */
    public function readMail($num = 1){
        if($this->showBuf)print($this->mess['mail_read']);
        $func = "retrieve_".$this->mode;
        $body = $this->$func($num);
        if(is_string($body)){
            return $body;
        }else{
            $this->error($this->emess["read"]);
            return false;
        }
    }
    
    /**
     * メール削除
     * @param int $num
     * @return boolean
     */
    public function deleteMail($num = 1){
        if($this->showBuf)print($this->mess['mail_delete']);
        if(is_int($num)){
            
        }
        return false;
    }
    /**
     * セッション切断
     */
    public function close(){
        if($this->showBuf)print($this->mess['close']);
        $func = "close_".$this->mode;
        if($this->$func()){
            return true;
        }else{
            $this->error($this->emess["close"]);
            return false;
        }
    }

    /**
    * サーバーへ接続
    * @param string $host
    * @param int $port
    * @return boolean
    */
    private function connect($host,$port){
        $this->sess = fsockopen($host,$port,$err,$errno,10);
        $this->bcheck = true;
        if(!$this->check_response()){
            return false;
        }
        return true;
    }
    /**
    **  ログイン
    **/
    private function login(){
      $func = "open".$this->mode;
      return $this->$func();
    }
    /**
    **   POP3でログイン
    **/
    private function openpop3(){

  $this->setBuf("USER {$this->user}\r\n");
    if(!$this->check_response()){
        return false;
    }

    $this->setBuf("PASS {$this->pass}\r\n");
    if(!$this->check_response()){
        return false;
    }
    
    return true;
  }
    /**
    **  Imapでログイン
    **/
    private function openimap(){
        $this->setBuf("? login {$this->user} {$this->pass}\r\n");
        if(!$this->check_response()){
            return false;
        }
        return true;
    }

 
    /**
     *  Pop3メール件数取得
     */
    private function status_pop3(){
        fputs($this->sess, "STAT \r\n");
        $hoge=$this->check_response();

        if( !$this->check_response() ) return false;
        $buf = fgets($this->sess);
        sscanf($buf, '+OK %d %d', $num, $size);

        return $num;
    }
  
    private $imap_mails = array();
    /**
     * Imapメール件数取得
     */
    private function status_imap(){
        #ボックス選択
        $this->setBuf("? select inbox\r\n");
        while($msg=fgets($this->sess)){

            if(strpos($msg,"? OK")!==false){
                break;
            }
        }
        #未読メール件数取得
        $list = array();
        /**
         * 未読 unseen
         * 既読 seen
         * 全件 ALL
         */
        $this->setBuf("? search unseen\r\n");
        while($msg=fgets($this->sess)){

          if(stripos($msg,"* SEARCH")!==false)$list = explode(" ",trim($msg));
          if(strpos($msg,"? OK")!==false)break;
        }
        $this->imap_mails = $list;
        $num = count($list);
        if($num < 3){
            return 0;
        }else{
            return $num - 2;
        }

    }

  
  /**
   * 特定番号のメール受信　POP3
   */
  private function retrieve_pop3($num){
      $default_timeout = ini_get('default_socket_timeout');
      stream_set_timeout($this->sess, 3);

      $this->setBuf("RETR {$num}\r\n");
      if( !$this->check_response() ) return false;

      $data = "";
      $line = "";
      while ( !feof($this->sess) ) {
          $meta_data = stream_get_meta_data($this->sess);
          if($meta_data["timed_out"]) break;
          
          $line = fgets($this->sess);
          if($line === false || preg_match("/^\.\r\n/", $line)) break;
          $line = preg_replace("/^\.\./", ".", $line);
          $data .= $line;
      }

      stream_set_timeout($this->sess, $default_timeout);

      return $data;
  }

  /**
   * 特定番号のメール受信　Imap
   */
  private function retrieve_imap($num){
    $debug = function($msg){
        if($this->showBuf){
            print("受信：".$msg."<br>");
        }        
    };
    
    #ボックス選択
    $this->setBuf("? select inbox\r\n");
    while($msg=fgets($this->sess)){
        $debug($msg);
        if(strpos($msg,"? OK")!==false){
            break;
        }
    }
    #未読メール件数取得
    $list = array();
    $this->setBuf("? search unseen\r\n");
    while($msg=fgets($this->sess)){
        $debug($msg);
        if(stripos($msg,"search")!==false)$list = explode(" ",trim($msg));
        if(strpos($msg,"? OK")!==false)break;
    }
    if(!count($list)) exit;
    $num = ($num <= 1)?0:$num-2;
    $num = $this->imap_mails[$num];
    
    $this->setBuf("? fetch {$num} body[]\r\n");
    $data = "";
    while($msg=fgets($this->sess)){
        $debug($msg);
        if(strpos($msg,"FETCH (")!==false) continue;
        if(strpos($msg,"FLAGS (")!==false) continue;
        if(strpos($msg,"? OK")!==false) break;
        $data .= $msg;
    }
    return $data;
  }

  //削除
  private function delete($num=1){
      fputs($this->sess, "DELE {$num}\r\n");
      if( !$this->check_response() ) return false;
  }

  //POPログアウト
  public function close_pop3(){
      $this->setBuf("QUIT\r\n");
      if( !$this->check_response() ) return false;
      fclose($this->sess);
      return true;
  }
    //Imapログアウト
    public function close_imap(){
        $this->setBuf("? logout\r\n");
        if( !$this->check_response() ) return false;
        fclose($this->sess);
        return true;
    }



    /**
    **　レスポンスチェック
    **/
    private function check_response(){
        $this->getBuf();
        if(strpos($this->buf,"OK") !== false){
          return true;
        }else{
          return false;
        }
    }
    private $bcheck = false;
    /**
     * 
     * @str type string
     * バッファに書き込み
     */
    private function setBuf($str){
        fwrite($this->sess,$str);
        $this->bcheck = true;
        if($this->showBuf){
            print($this->mess["send"].$str."<br>");
        }
    }
    /**
     * バッファを取得
     */
    private function getBuf(){
        if($this->bcheck){
            $this->buf = fgets($this->sess,512);
            $this->bcheck = false;
            if($this->showBuf){
                print($this->mess["get"].$this->buf."<br>");
            }
        }
    }
    
    /**
     * エラー投げ
     */
    private function error($error){
        throw new Exception($error);
    }
}

 ?>
