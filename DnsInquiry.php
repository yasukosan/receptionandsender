<?php

/*
 * 使えない
 * 理想：DNSサーバーを指定し、ドメインが引ける内容であるか、逆引き結果もあわせて検証
 * 現実：DNSサーバーからの応答をデコードまでは出来たがそこから先が不明レコードが取れない
 * 
 */


$dns = "192.168.2.173";
$domain = "yahoo.co.jp.";
$hoge = has_ns_records($domain,$dns);
var_dump($hoge);


function has_ns_records($domain, $dns)
{
    $timeout = 2;
    
    $data = pack( 'n6', rand(10, 77), 0x0100, 1, 0, 0, 0);
    #$data = rand( 10, 77 ) . "\1\0\0\1\0\0\0\0\0\0";
    #$data .= pack('n', '1') . pack('n', '0') . pack('n', '0') . pack('n', '0');
    foreach(explode('.', $domain) as $part)
    {
        $length = strlen($part);
        $data .= chr($length).$part;
        #$data.="{$bitso[$length]}".$part;
    }
    
    $data .= pack( 'n2' , 2, 1 );  // QTYPE=NS, QCLASS=IN
    #$data .= "\0\0" . pack('n', '2') . pack('n', '1');
    
    
    try {
        $errno = $errstr = 0;
        $fp = fsockopen( 'udp://' . $dns, 53, $errno, $errstr, $timeout );
        if (!$fp || !is_resource($fp)) return $errno;
        
        
        socket_set_timeout( $fp, $timeout );
        fwrite( $fp, $data );

        $response_data = fgets( $fp, 8192 );
        fclose( $fp );
        
    } catch (Exception $exc) {
       echo $exc->getTraceAsString();
       fclose( $fp );
    }

    $aaa = new DNS();
    $hoge = $aaa->parseRequest($response_data);
var_dump($hoge);

    // read answer header
    $ans_header = unpack( "nid/nspec/nqdcount/nancount/nnscount/narcount", substr( $response_data, 0, 12 ) );

    // skip question part
    $offset = strlen( $domain ) + 4 + 2 + 1; // 4 => QTYPE + QCLASS, 2 => len, 1 => null terminator

    $record_header = unpack("ntype/nclass/Nttl/nlength", substr( $response_data, 12 + $offset, 10 ) );

    // Expect type NS and class IN, when domain not exist it returns SOA (6)
    return $record_header[ 'type' ] === 2 && $record_header[ 'class' ] === 1;
}

class DNS
{
    private $_data;

    private $_type = array(
        '1' => 'A',
        '2' => 'NS',
        '5' => 'CNAME',
        '6' => 'SOA',
        '12' => 'PTR',
        '15' => 'MX'
    );

    private $_class = array(
        '1' => 'IN'
    );

    public function parseRequest($request)
    {
        $head = unpack('n*', substr($request, 0, 12));
        $body = substr($request, 12);
        
        $bindata = unpack("C*", $body);
        $ret     = "";
        foreach($bindata as $v){
                $ret    .= sprintf("%02x ",$v);
        }
        var_dump(hex2bin($ret));
        
        // result
        $data = array(
            'head' => array(
                'id' => $head[1],
                'flag' => array(
                    'qr' => ($head[2] & 32768)>>15,
                    'opcode' => ($head[2] & 30720)>>11,
                    'aa' => ($head[2] & 1024)>>10,
                    'tc' => ($head[2] & 512)>>9,
                    'rd' => ($head[2] & 256)>>8,
                    'ra' => ($head[2] & 128)>>7,
                    'z' => ($head[2] & 112)>>4,
                    'rcode' => ($head[2] & 15)
                ),
                'qdcount' => $head[3],
                'ancount' => $head[4],
                'nscount' => $head[5],
                'arcount' => $head[6]
            ),
            'question' => array(),
            'anser' => array(),
        );

        $body_arr = array_values(unpack('C*', $body));
        
        $i = 0;
        for ($j=0; $j<$data['head']['qdcount']; $j++) {
            $question = array(
                'qname' => '',
                'qtype' => 0,
                'qclass' => 0

            );
            while (true) {
                if (!isset($body_arr[$i]) || $body_arr[$i]==0) {
                    break;
                }
                list(,$len) = unpack('h', $body_arr[$i]);
                $question['qname'] .= substr($body, $i+1, $len).'.';
                $i += $len + 1;
            }
            list(,$question['qtype']) = unpack('v', substr($body, $i, 2));
            $i += 2;
            list(,$question['qclass']) = unpack('v', substr($body, $i, 2));
            $i += 2;
            $data['question'][] = $question;
        }
        
        $i = 0;
        for ($j=0; $j<$data['head']['ancount']; $j++) {
            $anser = array(
                'qname' => '',
                'qtype' => 0,
                'qclass' => 0

            );
            while (true) {
                if (!isset($body_arr[$i]) || $body_arr[$i]==0) {
                    break;
                }
                list(,$len) = unpack('h', $body_arr[$i]);
                $anser['qname'] .= substr($body, $i+1, $len).'.';
                $i += $len + 1;
            }
            list(,$anser['qtype']) = unpack('v', substr($body, $i, 2));
            $i += 2;
            list(,$anser['qclass']) = unpack('v', substr($body, $i, 2));
            $i += 2;
            $data['anser'][] = $anser;
        }

        $this->_data = $data;
        return $data;
    }
}
?>